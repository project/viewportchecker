<?php

namespace Drupal\viewportchecker\Commands;

use Drush\Commands\DrushCommands;
use Drush\Drush;
use Drush\Exec\ExecTrait;
use Symfony\Component\Filesystem\Filesystem;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class Drush9Commands extends DrushCommands {

  /**
   * Download and install the jQuery-viewport-checker library.
   *
   * @command viewportchecker:library
   * @aliases vpc-dl
   * @usage viewportchecker:library
   */
  public function library(): void {
    $path = \getcwd() . '/libraries/jquery.viewportchecker';
    $dist_path = $path . '/dist';
    $src_path = $path . '/src';
    $lib_name_min = 'jquery.viewportchecker.min.js';
    $lib_name = 'jquery.viewportchecker.js';
    $vpc_download_uri_min = 'https://raw.githubusercontent.com/ivnish/viewport-checker/master/dist/jquery.viewportchecker.min.js';
    $vpc_download_uri = 'https://raw.githubusercontent.com/ivnish/viewport-checker/master/src/jquery.viewportchecker.js';

    // Create the path if it does not exist.
    if (!\is_dir($path)) {
      \drush_op('mkdir', $path);
      $this->output()->writeln('Directory libraries/jquery.viewportchecker was created');
    }

    // Download jQuery-viewport-checker library only if path is writable.
    if (\is_writable($path)) {
      if (!\is_dir($dist_path)) {
        \drush_op('mkdir', $dist_path);
      }

      if (!\is_dir($src_path)) {
        \drush_op('mkdir', $src_path);
      }

      // Set the directory to the download location.
      \chdir($dist_path);

      $downloadsuccess = $this->drushDownloadFile($vpc_download_uri_min, '', '', $lib_name_min);
      if (!$downloadsuccess) {
        $this->output()->writeln('Cant download the library ' . $lib_name_min);
      }
      else {
        $this->output()->writeln('The library ' . $lib_name_min . ' was successfully downloaded!');
      }

      // Set the directory to the download location.
      \chdir($src_path);

      $downloadsuccess = $this->drushDownloadFile($vpc_download_uri, '', '', $lib_name);
      if (!$downloadsuccess) {
        $this->output()->writeln('Cant download the library ' . $lib_name);
      }
      else {
        $this->output()->writeln('The library ' . $lib_name . ' was successfully downloaded!');
      }
    }
    else {
      $this->output()->writeln('Drush was unable to install the jQuery-viewport-checker library because path is not writable.');
    }
  }

  /**
   * Downloads a file.
   *
   * Optionally uses user authentication, using either wget or curl,
   * as available.
   */
  public function drushDownloadFile(string $url, ?string $user = NULL, ?string $password = NULL, ?string $destination = NULL, bool $overwrite = TRUE): ?string {
    static $use_wget;
    if ($use_wget === NULL) {
      $use_wget = ExecTrait::programExists('wget');
    }
    $destination_tmp = \drush_tempnam('download_file');
    if ($use_wget) {
      $args = ['wget', '-q', '--timeout=30'];
      if ($user && $password) {
        $args = \array_merge($args, [
          "--user=$user",
          "--password=$password",
          '-O',
          $destination_tmp,
          $url,
        ]);
      }
      else {
        $args = \array_merge($args, ['-O', $destination_tmp, $url]);
      }
    }
    else {
      $args = ['curl', '-s', '-L', '--connect-timeout 30'];
      if ($user && $password) {
        $args = \array_merge($args, [
          '--user',
          "$user:$password",
          '-o',
          $destination_tmp,
          $url,
        ]);
      }
      else {
        $args = \array_merge($args, ['-o', $destination_tmp, $url]);
      }
    }
    $process = Drush::process($args);
    $process->mustRun();
    if (!Drush::simulate()) {
      if (!\drush_file_not_empty($destination_tmp) && $file = @\file_get_contents($url)) {
        @\file_put_contents($destination_tmp, $file);
      }
      if (!\drush_file_not_empty($destination_tmp)) {
        // Download failed.
        throw new \Exception(\dt("The URL !url could not be downloaded.", ['!url' => $url]));
      }
    }
    if ($destination) {
      $fs = new Filesystem();
      $fs->rename($destination_tmp, $destination, $overwrite);
      return $destination;
    }
    return $destination_tmp;
  }

}
