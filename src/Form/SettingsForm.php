<?php

namespace Drupal\viewportchecker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure jQuery-viewport-checker settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'viewportchecker_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['viewportchecker.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('viewportchecker.settings');

    $form['library'] = [
      '#type' => 'fieldset',
      '#title' => 'Load library settings',
    ];
    $form['library']['viewportchecker_devel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use uncompressed library'),
      '#description' => $this->t('Load the uncompressed version of viewportchecker. This SHOULD NOT be checked on production sites.'),
      '#default_value' => $config->get('viewportchecker_devel'),
    ];
    $form['library']['viewportchecker_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %user-wildcard for every user page. %front is the front page.", [
        '%user-wildcard' => '/user/*',
        '%front' => '<front>',
      ]),
      '#default_value' => $config->get('viewportchecker_paths'),
    ];
    $form['library']['viewportchecker_pathscondition'] = [
      '#type' => 'radios',
      '#options' => [
        'include' => $this->t('Show for the listed pages'),
        'exclude' => $this->t('Hide for the listed pages'),
      ],
      '#default_value' => $config->get('viewportchecker_pathscondition'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('viewportchecker.settings')
      ->set('viewportchecker_devel', $form_state->getValue('viewportchecker_devel'))
      ->set('viewportchecker_paths', $form_state->getValue('viewportchecker_paths'))
      ->set('viewportchecker_pathscondition', $form_state->getValue('viewportchecker_pathscondition'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
