# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

jQuery-viewport-checker is a Drupal integration module of the 
jQuery-viewport-checker javascript library. Little script that detects if an
element is in the viewport and adds a class to it.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/viewportchecker>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/viewportchecker>


## REQUIREMENTS

This module requires no modules outside of Drupal core.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Download the libraries manually or use "drush vpc-dl" command.
    3. Navigate to Administration > Configure > Development > 
       jQuery-viewport-checker to configure the module.
    4. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
